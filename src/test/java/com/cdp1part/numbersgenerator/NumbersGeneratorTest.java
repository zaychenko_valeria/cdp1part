/*
 * Copyright (c) 2018 Tideworks Technology, Inc.
 */

package com.cdp1part.numbersgenerator;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NumbersGeneratorTest {

    private INumbersGeneratorService numbersGeneratorService = new NumbersGeneratorService();

    @Test
    public void generateNumberNotNull() {
        assertNotNull(numbersGeneratorService.generateNumber());
    }

    @Test
    public void generateDifferent() {
        assertNotEquals(numbersGeneratorService.generateNumber(), numbersGeneratorService.generateNumber());
    }
}
