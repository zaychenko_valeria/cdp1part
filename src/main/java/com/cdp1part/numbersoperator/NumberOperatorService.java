/*
 * Copyright (c) 2018 Tideworks Technology, Inc.
 */

package com.cdp1part.numbersoperator;

public class NumberOperatorService implements INumbersOperatorService {

    public int sum(final int number1, final int number2) {
        return number1 + number2;
    }
}
