package com.cdp1part.numbersoperator;

import org.springframework.stereotype.Service;

@Service
public interface INumbersOperatorService {

    int sum(int number1, int number2);
}
