/*
 * Copyright (c) 2018 Tideworks Technology, Inc.
 */

package com.cdp1part.numbersgenerator;

import java.util.Random;

public class NumbersGeneratorService implements INumbersGeneratorService {

    @Override
    public int generateNumber() {
        Random random = new Random();
        return random.nextInt(100);
    }
}
