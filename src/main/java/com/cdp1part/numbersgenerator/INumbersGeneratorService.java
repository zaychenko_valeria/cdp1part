package com.cdp1part.numbersgenerator;

import org.springframework.stereotype.Service;

@Service
public interface INumbersGeneratorService {

    int generateNumber();
}
