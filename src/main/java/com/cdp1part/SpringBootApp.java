package com.cdp1part;

import com.cdp1part.numbersgenerator.INumbersGeneratorService;
import com.cdp1part.numbersgenerator.NumbersGeneratorService;
import com.cdp1part.numbersoperator.INumbersOperatorService;
import com.cdp1part.numbersoperator.NumberOperatorService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApp implements CommandLineRunner {

    @Override
    public void run(String... args) {
        INumbersGeneratorService numbersGeneratorService = new NumbersGeneratorService();
        INumbersOperatorService numbersOperatorService = new NumberOperatorService();

        int number1 = numbersGeneratorService.generateNumber();
        int number2 = numbersGeneratorService.generateNumber();

        System.out
                .println(String.format("%s + %s = %s", number1, number1, numbersOperatorService.sum(number1, number2)));
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootApp.class, args);
    }
}
